# README #

### Utilité du dépôt ? ###

Ce dépôt contient l'exercice demandé par l'entreprise Talentroc.

Cet exercice consistait à créer un carnet d'adresse en utilisant les technologies suivantes :

-une technologie WEB

-Java

-le framework Tapestry

### Comment le lancer? ###

Ce projet n'a pas pour but d'être exécuté directement.

Il est là pour que le code puisse être analysé.

Cependant, si vous souhaitez l'exécuter, vous pouvez l'importer dans eclipse(Java2EE), c'est un projet Maven .

Pour l'exécuter :

-Click droit -> runAs -> MavenBuild ...

Onglet "goals" -> entrer "jetty:run" -> Apply -> Run

Pour l'instant le projet ne contient pas le pattern DAO normalement utilisé pour ce genre d'application .

La prise en main du framework Tapestry a été plus longue et laborieuse que prévue et j'ai jugé qu'il était nécessaire de rendre ce projet dans un délais maximum d'une semaine .