package projetCarnet.CarnetAdresse.pages.personne;



import java.util.List;

import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.hibernate.Session;

import projetCarnet.CarnetAdresse.entities.Personne;
import projetCarnet.CarnetAdresse.pages.Index;

/**
 * Classe CreatePersonne
 * 	Cette classe permet de cr�er une personne et de l'ajouter dans la base de donn�es.
 */
public class CreatePersonne
{
	@InjectPage
	private Index index;
	
	@Inject
	private Session session;
	
	@Property
	private Personne personne;
	

	/**
	 * M�thode qui est appel�e lors de la validation du formulaire.
	 * @return un objet de type Index.
	 */
	@CommitAfter
	Object onSuccess(){
		//session.saveOrUpdate(personne);
		session.persist(personne);
		return index;
	}
	
	/**
	 * M�thode permettant de modifier une personne si son ID est pass� en parametre .
	 * Pour simplifier la tache, ici on supprimera la personne correspondante avant de la r�ins�rer dans la table.
	 * @userId represente l'id la personne que l'on veut modifier.
	 */
	void onActivate(long userId)
    {
		List<Personne> lp = session.createCriteria(Personne.class).list();
		for(Personne p:lp){
        	if(p.getId()==userId){
        		personne = p;
        		index.onActionFromDelete(personne);
        	}
        }
		
    }
	

}
