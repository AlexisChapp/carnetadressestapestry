package projetCarnet.CarnetAdresse.pages.personne;




import java.util.List;

import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.hibernate.Session;

import projetCarnet.CarnetAdresse.entities.Personne;
import projetCarnet.CarnetAdresse.pages.Index;

/**
 * Start page of application CarnetAdresse.
 */
public class Vue
{
	@InjectPage
	private Index index;
	
	@Inject
	private Session session;
	
	@Property
	private Personne personne;

	/**
	 * M�thode permettant d'afficher les propri�t�es d'une personne si son ID est pass� en parametre .
	 * @userId represente l'id la personne que l'on veut modifier.
	 */
	void onActivate(long userId)
    {
		List<Personne> lp = session.createCriteria(Personne.class).list();
		for(Personne p:lp){
        	if(p.getId()==userId){
        		personne = p;
        	}
        }
    }
	

}
