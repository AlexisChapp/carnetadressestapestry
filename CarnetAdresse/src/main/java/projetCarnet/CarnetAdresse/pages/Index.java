package projetCarnet.CarnetAdresse.pages;



import java.util.List;

import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

import projetCarnet.CarnetAdresse.entities.Personne;

/**
 * Page Index.
 */
public class Index
{
	@Property
	private Personne personne;
	
	@Property
	private Personne p;
	
	
	@Inject
	private Session session;
	
	/**
	 * M�thode permettant de r�cup�rer la liste de personnes contenue dans la base de donn�es
	 * @return une List de Personne
	 */
	public List<Personne> getPersonnes(){
		return session.createCriteria(Personne.class).list();
	}
	
	
	/**
	 * M�thode permettant de supprimer une personne de la base de donn�es
	 * @param p est de type Personne et repr�sente la personne que l'on veut supprimer de la base.
	 */
	@CommitAfter
	public void onActionFromDelete(Personne p){
		session.delete(p);
	
	}
	
	/**
	 * M�thode permettant de supprimer une personne de la base de donn�es en fonction de son ID
	 * @param userId est de type Long et repr�sente l'id de la personne que l'on veut supprimer de la base.
	 */
	 void onActionFromDelete(long userId)
	    {
	        List<Personne> lp = session.createCriteria(Personne.class).list();
	        for(Personne p:lp){
	        	if(p.getId()==userId){
	        		session.delete(p);
	        	}
	        }
	    }
	
	
	@InjectPage
	private Index index;
	

	
}
